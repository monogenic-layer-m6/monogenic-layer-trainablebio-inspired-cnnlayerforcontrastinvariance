#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"


import os
import h5py
import numpy as np


def create_directory(dir_name):
    try:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
    except IOError:
        raise


def load_hdf5(full_path):
    print('Loading data from', full_path, '...', end='')
    hf = h5py.File(full_path, 'r')
    x = np.array(hf.get('x'))
    y = np.array(hf.get('y'))
    hf.close()
    print("finished")
    return x, y