#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"


import os
import argparse

from executors.executorcontrasttest import TestExecutor
from executors.executorcontrasttraining import TrainExecutor

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Trainable Bio-inspired CNN Layer for Contrast Invariance.')
    parser.add_argument('--result', '-r', required=True, help='Results directory')
    parser.add_argument('--run-type', '-t', default='train', type=str, help='Execution type [train|eval] (default '
                                                                            'train)')
    parser.add_argument('--epochs', '-e', default=100, type=int, help='Epochs used in train process (default 100)')
    parser.add_argument('--lr', '-l', default=0.001, type=float, help='Learning rate (default 0.001)')
    parser.add_argument('--batch', '-b', default=128, type=int, help='Batch size (default 128)')
    parser.add_argument('--technique-type', '-c', default='tf', type=str, help='Technique [tf|scale] (default tf)')
    args = parser.parse_args()
    data_config = {
        # Dataset name | Classes | Image size | Split size
        'mnist': ('mnist', 10, (28, 28, 1), ['train[:80%]', 'train[80%:]', 'test']),
        'fashion_mnist': ('fashion_mnist', 10, (28, 28, 1), ['train[:80%]', 'train[80%:]', 'test']),
        'cifar10': ('cifar10', 10, (32, 32, 3), ['train[:80%]', 'train[80%:]', 'test']),
        'cats_vs_dogs': ('cats_vs_dogs', 2, (128, 128, 3), ['train[:70%]', 'train[70%:85%]', 'train[85%:]'])
    }

    if args.run_type == 'train':
        trainer = TrainExecutor(data_config=data_config, output_folder_name=args.result, epochs=args.epochs, lr=args.lr,
                                batch_size=args.batch, tech_type=args.technique_type)
        trainer.execute()
    elif args.run_type == 'eval':
        tester = TestExecutor(data_config=data_config, output_folder_name=args.result, lr=args.lr,
                              batch_size=args.batch, tech_type=args.technique_type)
        tester.execute()

    print('Done!')
