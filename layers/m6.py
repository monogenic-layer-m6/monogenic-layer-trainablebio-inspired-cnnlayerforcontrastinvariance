#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"

import tensorflow as tf

from tensorflow.keras.initializers import Initializer
from tensorflow.keras.layers import Layer


class M6(Layer):

    def __init__(self, f=1., s=1., wl=1., c=.33, **kwargs):
        super(M6, self).__init__(**kwargs)
        self.f = self.add_weight(name='f', shape=(1,), initializer=Constant(value=f))
        self.s = self.add_weight(name='s', shape=(1,), initializer=Constant(value=s))
        self.wl = self.add_weight(name='wl', shape=(1,), initializer=Constant(value=wl))
        self.c = self.add_weight(name='c', shape=(1,), initializer=Constant(value=c))

    def call(self, inputs, **kwargs):
        x = tf.reduce_mean(inputs, axis=-1)
        _, cols, rows, chn = inputs.shape
        m = self.monogenic_scale(cols=cols, rows=rows, s=self.s, wl=self.wl, f=self.f, c=self.c)
        im = tf.signal.fft3d(tf.cast(x, tf.complex64))
        imf = im * m[..., 0]
        imh1 = im * m[..., 1]
        imh2 = im * m[..., 2]
        f = tf.math.real(tf.signal.ifft3d(imf))
        h1 = tf.math.real(tf.signal.ifft3d(imh1))
        h2 = tf.math.real(tf.signal.ifft3d(imh2))
        ori = tf.atan(tf.math.divide_no_nan(-h2, h1))
        fr = tf.sqrt(h1 ** 2 + h2 ** 2) + 0.001
        ft = tf.atan2(f, fr)
        ones = tf.ones_like(x)
        fts = self.scale_max_min(ft)
        frs = self.scale_max_min(fr)
        oris = self.scale_max_min(ori)
        hsv_tensor_v = tf.stack((fts, frs, ones), axis=-1)
        rgb_tensor_v = self.hsv_to_rgb(hsv_tensor_v)
        hsv_tensor_o = tf.stack((oris, frs, ones), axis=-1)
        rgb_tensor_o = self.hsv_to_rgb(hsv_tensor_o)
        rgb_tensor = tf.concat([rgb_tensor_o, rgb_tensor_v], axis=-1)
        return rgb_tensor

    def get_config(self):
        config = super().get_config().copy()
        config.update({'s': self.s, 'wl': self.wl, 'f': self.f, 'c': self.c})
        return config

    @classmethod
    def hsv_to_rgb(cls, tensor):
        h = tensor[..., 0]
        s = tensor[..., 1]
        v = tensor[..., 2]
        c = s * v
        m = v - c
        dh = h * 6
        h_category = tf.cast(dh, tf.int32)
        fmodu = dh % 2
        x = c * (1 - tf.abs(fmodu - 1))
        component_shape = tf.shape(tensor)[:-1]
        dtype = tensor.dtype
        rr = tf.zeros(component_shape, dtype=dtype)
        gg = tf.zeros(component_shape, dtype=dtype)
        bb = tf.zeros(component_shape, dtype=dtype)
        h0 = tf.equal(h_category, 0)
        rr = tf.where(h0, c, rr)
        gg = tf.where(h0, x, gg)
        h1 = tf.equal(h_category, 1)
        rr = tf.where(h1, x, rr)
        gg = tf.where(h1, c, gg)
        h2 = tf.equal(h_category, 2)
        gg = tf.where(h2, c, gg)
        bb = tf.where(h2, x, bb)
        h3 = tf.equal(h_category, 3)
        gg = tf.where(h3, x, gg)
        bb = tf.where(h3, c, bb)
        h4 = tf.equal(h_category, 4)
        rr = tf.where(h4, x, rr)
        bb = tf.where(h4, c, bb)
        h5 = tf.equal(h_category, 5)
        rr = tf.where(h5, c, rr)
        bb = tf.where(h5, x, bb)
        r = rr + m
        g = gg + m
        b = bb + m
        return tf.stack([r, g, b], axis=-1)

    @classmethod
    def mesh_range(cls, size):
        if len(size) == 1:
            rows = cols = size
        else:
            rows, cols = size
        if cols % 2:
            x_values = tf.range(-(cols - 1) / 2., ((cols - 1) / 2.) + 1) / float(cols - 1)
        else:
            x_values = tf.range(-cols / 2., cols / 2.) / float(cols)
        if rows % 2:
            y_values = tf.range(-(rows - 1) / 2., ((rows - 1) / 2.) + 1) / float(rows - 1)
        else:
            y_values = tf.range(-rows / 2., rows / 2.) / float(rows)
        return tf.meshgrid(x_values, y_values)

    def low_pass_filter(self, size, cutoff, n):
        x, y = self.mesh_range(size)
        radius = tf.sqrt(x * x + y * y)
        lpf = tf.cast(tf.signal.ifftshift(1. / (1. + (radius / cutoff) ** (2. * n))), tf.complex64)
        return lpf

    def meshs(self, size):
        x, y = self.mesh_range(size)
        radius = tf.cast(tf.signal.ifftshift(tf.sqrt(x * x + y * y)), tf.complex64)
        x = tf.cast(tf.signal.ifftshift(x), tf.complex64)
        y = tf.cast(tf.signal.ifftshift(y), tf.complex64)
        return x, y, radius

    def riesz_trans(self, cols, rows):
        u1, u2, qs = self.meshs((rows, cols))
        qs = tf.cast(tf.sqrt(u1 * u1 + u2 * u2), tf.complex64)
        indices = tf.constant([[0, 0]])
        updates = tf.constant([1.], tf.complex64)
        q = tf.tensor_scatter_nd_update(qs, indices, updates)
        h1 = (1j * u1) / q
        h2 = (1j * u2) / q
        return h1, h2

    @classmethod
    def scale_max_min(cls, x):
        x_min = tf.reduce_min(x, axis=(1, 2), keepdims=True)
        x_max = tf.reduce_max(x, axis=(1, 2), keepdims=True)
        scale = tf.math.divide_no_nan(
            tf.subtract(x, x_min),
            tf.subtract(x_max, x_min)
        )
        return scale

    def log_gabor_scale(self, cols, rows, s, wl, f, c):
        u1, u2, rad_f = self.meshs((rows, cols))
        indices = tf.constant([[0, 0]])
        updates = tf.constant([1.], tf.complex64)
        radius = tf.tensor_scatter_nd_update(rad_f, indices, updates)
        lp = self.low_pass_filter((rows, cols), .45, 15)
        log_gabor_denominator = tf.cast(2. * tf.math.log(c) ** 2., tf.complex64)
        wave_length = wl * f ** s
        fo = tf.constant(1., dtype=tf.float32) / wave_length
        log_rad_over_fo = (tf.math.log(radius / tf.cast(fo, tf.complex64)))
        log_gabor = tf.exp(-(log_rad_over_fo * log_rad_over_fo) / log_gabor_denominator)
        log_gabor = (lp * log_gabor)
        return log_gabor

    def monogenic_scale(self, cols, rows, s, wl, f, c):
        h1, h2 = self.riesz_trans(cols, rows)
        lg = self.log_gabor_scale(cols, rows, s, wl, f, c)
        lg_h1 = lg * h1
        lg_h2 = lg * h2
        m = tf.stack([lg, lg_h1, lg_h2], axis=-1)
        return m


class Constant(Initializer):

    def __init__(self, value):
        self.value = value

    def __call__(self, shape, dtype=tf.float32):
        return tf.Variable([self.value], dtype=dtype)
