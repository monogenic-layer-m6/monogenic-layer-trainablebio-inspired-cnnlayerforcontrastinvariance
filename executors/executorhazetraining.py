#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"

import gc
import pandas as pd
import tensorflow as tf

from os.path import join
from tensorflow.keras.callbacks import History
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from callbacks.m6callback import M6CallBack
from executors.executorhaze import Executor
from models.modelfactory import ModelFactory
from utils.utils import load_hdf5


class TrainExecutor(Executor):
    checkpoint_config = dict(
        monitor='val_acc',
        verbose=1,
        save_best_only=True,
        save_weights_only=True,
        mode='auto',
        save_freq='epoch'
    )

    def __init__(self, data_config, output_folder_name, epochs, lr, batch_size):
        super(TrainExecutor, self).__init__(data_config=data_config, output_folder_name=output_folder_name,
                                            epochs=epochs, lr=lr, batch_size=batch_size)

    def get_data(self):
        file_name = '{}_{}_3-{}-{}.h5'.format(self.data_name, self.level, self.input_shape[0], self.input_shape[1])
        file_name = join('data_haze', file_name)
        x, y = load_hdf5(full_path=file_name)
        train_len = int(x.shape[0] * (self.split[0]))
        validation_len = int(x.shape[0] * (self.split[1]))
        x_train, y_train = x[0:train_len], y[0:train_len]
        x_validation, y_validation = x[train_len:train_len+validation_len], y[train_len:train_len+validation_len]
        train_generator = ImageDataGenerator(rescale=1. / 255)
        train_generator.fit(x_train)
        train = train_generator.flow(
            x_train,
            y_train,
            batch_size=self.batch_size
        )
        validation_generator = ImageDataGenerator(rescale=1. / 255)
        validation_generator.fit(x_validation)
        validation = validation_generator.flow(
            x_validation,
            y_validation,
            batch_size=self.batch_size
        )
        return train, validation

    def process(self, data):
        train, validation = data
        model = ModelFactory.instance(test_type=self.test_type,
                                      input_shape=self.input_shape,
                                      num_classes=self.num_classes)
        print(model.summary())
        model.compile(optimizer=Adam(lr=self.lr), loss=sparse_categorical_crossentropy, metrics=['acc'])
        execution_name = '{}-{}-{}'.format(self.data_name, int(self.level), self.test_type)
        checkpoint = ModelCheckpoint(join(self.result_path, execution_name + '.h5'), **self.checkpoint_config)
        m6_callback = M6CallBack(model)
        print('Model name: {}'.format(execution_name))
        history = model.fit(
            train, epochs=self.epochs, validation_data=validation,
            callbacks=[m6_callback, checkpoint, History()],
            verbose=1
        )
        df_history = pd.DataFrame(history.history)
        df_history.to_csv(join(self.result_path, 'history_{}.csv').format(execution_name), index=False)
        weights = m6_callback.get_weights()
        if weights.any():
            df = pd.DataFrame(weights)
            df.columns = ['epoch', 'scaling', 'scale', 'wavelength', 'sigma']
            df.to_csv(join(self.result_path, 'weights_{}.csv'.format(execution_name)), index=False)
        del model
        tf.keras.backend.clear_session()
        gc.collect()
