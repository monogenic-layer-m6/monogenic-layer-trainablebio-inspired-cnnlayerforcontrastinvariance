#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"


import gc
import pandas as pd
import tensorflow as tf
import tensorflow_datasets as tfds

from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import History
from callbacks.m6callback import M6CallBack
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import sparse_categorical_crossentropy

from os.path import join
from executors.executorcontrast import Executor
from models.modelfactory import ModelFactory


class TrainExecutor(Executor):

    checkpoint_config = dict(
        monitor='val_accuracy',
        verbose=1,
        save_best_only=True,
        save_weights_only=True,
        mode='auto',
        save_freq='epoch'
    )

    def __init__(self, data_config, output_folder_name, epochs, lr, batch_size, tech_type):
        super(TrainExecutor, self).__init__(data_config=data_config, output_folder_name=output_folder_name,
                                            epochs=epochs, lr=lr, batch_size=batch_size, tech_type=tech_type)

    def get_data(self):
        (data_train, data_validation, _) = tfds.load(name=self.data_name, split=self.split, as_supervised=True)
        train = data_train.map(self.technique_types[self.tech_type]).map(self.format_image).batch(self.batch_size)
        validation = data_validation.map(self.technique_types[self.tech_type]).map(self.format_image).batch(self.batch_size)
        return train, validation

    def process(self, data):
        train, validation = data
        model = ModelFactory.instance(test_type=self.test_type,
                                      input_shape=self.input_shape,
                                      num_classes=self.num_classes)
        print(model.summary())
        model.compile(optimizer=Adam(lr=self.lr), loss=sparse_categorical_crossentropy, metrics=['accuracy'])
        execution_name = '{}-{}-{}'.format(self.data_name, int(self.level * 100), self.test_type)
        checkpoint = ModelCheckpoint(join(self.result_path, execution_name + '.h5'), **self.checkpoint_config)
        m6_callback = M6CallBack(model)
        print('Model name: {}'.format(execution_name))
        history = model.fit(
            train, epochs=self.epochs, validation_data=validation,
            callbacks=[m6_callback, checkpoint, History()],
            verbose=1
        )
        df_history = pd.DataFrame(history.history)
        df_history.to_csv(join(self.result_path, 'history_{}.csv').format(execution_name), index=False)
        weights = m6_callback.get_weights()
        if weights.any():
            df = pd.DataFrame(weights)
            df.columns = ['epoch', 'scaling', 'scale', 'wavelength', 'sigma']
            df.to_csv(join(self.result_path, 'weights_{}.csv'.format(execution_name)), index=False)
        del model
        tf.keras.backend.clear_session()
        gc.collect()
