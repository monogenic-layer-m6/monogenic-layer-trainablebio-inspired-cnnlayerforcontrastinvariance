#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"

import gc
import os
import pandas as pd
import tensorflow as tf

from os.path import join
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from executors.executorhaze import Executor
from models.modelfactory import ModelFactory
from utils.utils import load_hdf5


class TestExecutor(Executor):

    def __init__(self, data_config, output_folder_name, lr, batch_size):
        super(TestExecutor, self).__init__(data_config=data_config, output_folder_name=output_folder_name,
                                           lr=lr, batch_size=batch_size)
        self.evaluation = [['Model', 'Level', 'Test', 'Test_acc', 'Test_loss']]

    def get_data(self):
        file_name = '{}_{}_3-{}-{}.h5'.format(self.data_name, self.level, self.input_shape[0], self.input_shape[1])
        file_name = join('data_haze', file_name)
        x, y = load_hdf5(full_path=file_name)
        train_len = int(x.shape[0] * (self.split[0]))
        validation_len = int(x.shape[0] * (self.split[1]))
        test_len = int(x.shape[0] * (self.split[2]))
        x_test, y_test = x[train_len+validation_len:train_len+validation_len+test_len],\
                         y[train_len+validation_len:train_len+validation_len+test_len]
        test_generator = ImageDataGenerator(rescale=1. / 255)
        test_generator.fit(x_test)
        test = test_generator.flow(
            x_test,
            y_test,
            batch_size=self.batch_size
        )
        return test

    def process(self, data):
        model_names = self.get_model_names(sorted(os.listdir(self.result_path)))
        for model_name in model_names:
            test_type, _ = os.path.splitext(model_name.split('-')[-1])
            data_name = model_name.split('-')[0]
            if self.data_name == data_name and self.test_type == test_type:
                model = ModelFactory.instance(test_type=self.test_type,
                                              input_shape=self.input_shape,
                                              num_classes=self.num_classes)
                model.load_weights(join(self.result_path, model_name))
                model.compile(optimizer=Adam(lr=self.lr), loss=sparse_categorical_crossentropy, metrics=['accuracy'])
                test_loss, test_acc = model.evaluate(data, verbose=1)
                self.evaluation.append([
                    model_name, str(self.level), self.test_type, str(test_acc), str(test_loss)]
                )
                print('\nModel: [{}] - accuracy [{}] - loss [{}]'.format(
                    model_name, test_acc, test_loss)
                )
                print(model_name, test_type, data_name)
                del model
                tf.keras.backend.clear_session()
                gc.collect()
        df = pd.DataFrame(self.get_evaluation())
        df.to_csv(join('{}', '{}_results.csv').format(self.output_directory_name, self.data_name), index=False,
                  header=False)

    def get_model_names(self, model_names):
        models = list()
        for name in model_names:
            if self.data_name in name and name.endswith('.h5'):
                models.append(name)
        return models

    def get_evaluation(self):
        return self.evaluation

    def clear(self):
        self.evaluation.clear()
        self.evaluation = [['Model', 'Level', 'Test', 'Test_acc', 'Test_loss']]
