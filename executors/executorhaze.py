#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"


import time
import numpy as np
import tensorflow as tf

from utils.utils import create_directory
from models.modelfactory import ModelFactory


class Executor(object):

    LEVELS = [0, 1, 2, 3]
    TEST_TYPE = [ModelFactory.CONV_TEST, ModelFactory.M6_TEST, ModelFactory.CONV_CONV_TEST, ModelFactory.M6_CONV_TEST,
                 ModelFactory.RESNET20_TEST, ModelFactory.M6_RESNET20_TEST]

    def __init__(self, data_config, output_folder_name, epochs=100, lr=0.001, batch_size=128):
        self.data_name = None
        self.num_classes = None
        self.input_shape = None
        self.split = None
        self.level = None
        self.test_type = None
        self.result_path = None
        self.data_config = data_config
        self.output_directory_name = output_folder_name
        self.epochs = epochs
        self.lr = lr
        self.batch_size = batch_size

    def execute(self):

        for data, config in self.data_config.items():
            self.clear()
            for level in Executor.LEVELS:
                for test_type in Executor.TEST_TYPE:

                    self.data_name, self.num_classes, self.input_shape, self.split = config
                    self.level = level
                    self.test_type = test_type

                    print('\nData name [{}] - Level [{}] - Test type [{}]'.format(
                        self.data_name,
                        level,
                        test_type)
                    )
                    if not self.test_available():
                        continue

                    self.result_path = '{}/{}_results'.format(self.output_directory_name, self.data_name)
                    create_directory(self.result_path)

                    data = self.get_data()
                    start_time = time.time()
                    self.process(data)
                    print('\nData name [{}] - Level [{}] - Test type [{}] - execution time [{}]'.format(
                        self.data_name,
                        level,
                        test_type,
                        time.time() - start_time)
                    )
                    print('='*20)

    def test_available(self):
        is_model = self.test_type == ModelFactory.RESNET20_TEST or self.test_type == ModelFactory.M6_RESNET20_TEST
        is_data_name = self.data_name == 'mnist' or self.data_name == 'fashion_mnist'
        available = is_model and is_data_name
        return not available

    def get_data(self):
        raise NotImplementedError('subclasses must implement get_data()')

    def process(self, data):
        raise NotImplementedError('subclasses must implement process()')

    def clear(self):  # Dummy method
        pass

    def format_image(self, image, label):
        image = tf.cast(image, tf.float32)
        image = (image / 255.)
        image = tf.image.resize(image, self.input_shape[:2])
        return image, label

    def change_contrast(self, image, label):
        image = tf.cast(image, tf.float32)
        image = image / 255.
        x_max = tf.reduce_max(image, axis=(1, 2), keepdims=True)
        x_min = tf.reduce_min(image, axis=(1, 2), keepdims=True)
        scale = self.level + tf.math.divide_no_nan(
            tf.subtract(image, x_min) * (1. - self.level),
            tf.subtract(x_max, x_min)
        )
        scale = scale * 255
        scale = tf.cast(scale, tf.uint8)
        return scale, label

    def change_contrast_preprocessing(self, image):
        image = image.astype(np.uint8)
        x_max = np.max(image, axis=(0, 1))
        x_min = np.min(image, axis=(0, 1))
        scale = self.level + np.divide(
            np.subtract(image, x_min) * (1. - self.level),
            np.subtract(x_max, x_min)
        )
        scale = scale.astype(np.float32)
        return scale
