# A TRAINABLE MONOGENIC CONVNET LAYER ROBUST IN FRONT OF LARGE CONTRAST CHANGES IN IMAGE CLASSIFICATION
## Abstract 
Convolutional Neural Networks (ConvNets) at present achieve remarkable performance in image classification tasks. However, current ConvNets cannot guarantee the capabilities of the mammalian visual systems such as invariance to contrast and illumination changes. Some ideas to overcome the illumination and contrast variations usually have to be tuned manually and tend to fail when tested with other types of data degradation. In this context, we present a new bio-inspired entry layer, M6, which detects low-level geometric features (lines, edges, and orientations) which are similar to patterns detected by the V1 visual cortex. This new trainable layer is capable of coping with image classification even with large contrast variations. The explanation for this behavior is the monogenic signal geometry, which represents each pixel value in a 3D space using quaternions, a fact that confers a degree of explainability to the networks. We compare M6 with a conventional convolutional layer (C) and a deterministic quaternion local phase layer (Q9). The experimental setup is designed to evaluate the robustness of our M6 enriched ConvNet model and includes three architectures, four datasets, three types of contrast degradation (including non-uniform haze degradations). The numerical results reveal that the models with M6 are the most robust in front of any kind of contrast variations. This amounts to a significant enhancement of the C models, which usually have reasonably good performance only when the same training and test degradation are used, except for the case of maximum degradation. Moreover, the Structural Similarity Index Measure (SSIM) is used to analyze and explain the robustness effect of the M6 feature maps under any kind of contrast degradations.

## Paper
https://arxiv.org/pdf/2109.06926.pdf 
## Requirements

To install requirements:

```setup
conda env create -f environment.yml
```

Creates a conda environment called `monogenic`. You can access this with the following command:

```condaenv
conda activate monogenic
```

## Project structure

```
- data_haze/ *
  - cad_haze_0_3-224-224.h5 *
  - cad_haze_1_3-224-224.h5 *
  - cad_haze_2_3-224-224.h5 *
  - cad_haze_3_3-224-224.h5 *
  - cifar_haze_0_3-32-32.h5 *
  - cifar_haze_1_3-32-32.h5 *
  - cifar_haze_2_3-32-32.h5 *
  - cifar_haze_3_3-32-32.h5 *
- callbacks/
  - m6callbacks.py
- executors/
  - executorcontrast.py
  - executorcontrasttest.py
  - executorcontrasttraining.py
  - executorhaze.py
  - executorhazetest.py
  - executorhazetraining.py
- layers/
  - m6.py
- models/
  - convconvmodel.py
  - convmodel.py
  - m6convmodel.py
  - m6model.py
  - modelfactory.py
  - resnet20model.py
- utils/
  - utils.py
- environment.yml
- README.md
- run_contrast.py
- run_haze.py
- show_results.py
```

_Note: (*) mean a set of files that must be downloaded manually. More instructions below._


## Training

The experiment is divided in two parts, the first dealing with `contrast` using TensorFlow and max-min scale techniques (`run_contrast.py`) and using the TensorFlow dataset library. The second is about applying `haze` to images (`run_haze.py`) and uses a custom h5 dataset (this dataset is currently available from a public Google buket). In both cases, a training, evaluation and results sample process must be generated.

## Contrast degradations

![contrast_min_max_scale](/uploads/2e58507abef85841c14b06fbf56eee3f/contrast_min_max_scale.png)![tf_contrast_change](/uploads/fd2da6c3218646a46cd0a83441a7631d/tf_contrast_change.png)

### Usage Contrast

```bash
Trainable Bio-inspired CNN Layer for Contrast Invariance.
usage: run_contrast.py [-h] --result RESULT [--run-type RUN_TYPE]
                       [--epochs EPOCHS] [--lr LR] [--batch BATCH]
                       [--technique-type TECHNIQUE_TYPE]

optional arguments:
  -h, --help            show this help message and exit
  --result RESULT, -r RESULT
                        Results directory
  --run-type RUN_TYPE, -t RUN_TYPE
                        Execution type [train|eval] (default train)
  --epochs EPOCHS, -e EPOCHS
                        Epochs used in train process (default 100)
  --lr LR, -l LR        Learning rate (default 0.001)
  --batch BATCH, -b BATCH
                        Batch size (default 128)
  --technique-type TECHNIQUE_TYPE, -c TECHNIQUE_TYPE
                        Technique [tf|scale] (default tf)

```

To train:

```bash
python run_contrast.py --result <path_to_data> --run-type train --epochs 100 --lr 0.001 --batch 128 --technique-type [tf|scale]
```

> Train example: python run_contrast.py --result results --run-type train --epochs 100 --lr 0.001 --batch 128 --technique-type [tf|scale]

To evaluate:
```bash
python run_contrast.py --result <path_to_data> --run-type eval --lr 0.001 --batch 128 --technique-type [tf|scale]
```
> Evaluation example: python run_contrast.py --result results --run-type eval --lr 0.001 --batch 128 --technique-type [tf|scale]

_Note: This script generates four csv files (one file per dataset) into `path_to_data` directory. Each csv contais the model name (created in training step), contrast level, CNN architecture type, eval accuracy and eval loss._


### Usage Haze

```bash
usage: Trainable Bio-inspired CNN Layer for Contrast Invariance.
       [-h] --result RESULT [--run-type RUN_TYPE] [--epochs EPOCHS] [--lr LR]
       [--batch BATCH]

optional arguments:
  -h, --help            show this help message and exit
  --result RESULT, -r RESULT
                        Results directory
  --run-type RUN_TYPE, -t RUN_TYPE
                        Execution type [train|eval] (default train)
  --epochs EPOCHS, -e EPOCHS
                        Epochs used in train process (default 100)
  --lr LR, -l LR        Learning rate (default 0.001)
  --batch BATCH, -b BATCH
                        Batch size (default 128)
```

IMPORTANT: In order to run the haze experiment download the [haze h5 dataset](https://storage.googleapis.com/dai-2020/haze/data_haze.tar.gz). The directory `data_haze` must be placed in the root path of the project.

You can download and uncompress as follows:

```bash
wget https://storage.googleapis.com/dai-2020/haze/data_haze.tar.gz
tar xf data_haze.tar.gz
```

To train:

```bash
python run_haze.py --result <path_to_data> --run-type train --epochs 100 --lr 0.001 --batch 128
```

> Train example: python run_haze.py --result results --run-type train --epochs 100 --lr 0.001 --batch 128

To evaluate:
```bash
python run_haze.py --result <path_to_data> --run-type eval --lr 0.001 --batch 128
```
> Evaluation example: python run_haze.py --result results --run-type eval --lr 0.001 --batch 128

_Note: This script generates four csv files (one file per dataset) into `path_to_data` directory. Each csv contais the model name (created in training step), contrast level, CNN architecture type, eval accuracy and eval loss._


### Scripts Outpus
- The script creates the `<path_to_data>` directory where the training results are stored.
- For the contrast experiment the datasets MNIST, Fashion-MNIST, CIFAR-10 and Cats vs dogs is downloaded through the [TensorFlow dataset](https://www.tensorflow.org/datasets/catalog/overview) package.
- For the haze experiment the dataset must be downloaded manually.


### Display results

These files are processed as follows:

```bash
python show_results.py --results <path_to_data> --output <output_file_name>
```
> example: python show_results.py --results results --output results/results.txt

The result is saved in the `output_file_name` file.


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

```license
MIT License

Copyright (c) 2020 E. Ulises Moya <dr.ulisesmoya@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
