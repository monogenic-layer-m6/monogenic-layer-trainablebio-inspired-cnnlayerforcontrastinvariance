#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"


import argparse

from executors.executorhazetest import TestExecutor
from executors.executorhazetraining import TrainExecutor

if __name__ == "__main__":
    parser = argparse.ArgumentParser('Trainable Bio-inspired CNN Layer for Haze Invariance.')
    parser.add_argument('--result', '-r', required=True, help='Results directory')
    parser.add_argument('--run-type', '-t', default='train', type=str, help='Execution type [train|eval] (default '
                                                                            'train)')
    parser.add_argument('--epochs', '-e', default=100, type=int, help='Epochs used in train process (default 100)')
    parser.add_argument('--lr', '-l', default=0.001, type=float, help='Learning rate (default 0.001)')
    parser.add_argument('--batch', '-b', default=128, type=int, help='Batch size (default 128)')
    args = parser.parse_args()
    data_config = {
        # Dataset name | Classes | Image size | Split size
        'cad_haze': ('cad_haze', 2, (224, 224, 3), [.7, .15, .15]),
        'cifar_haze': ('cifar_haze', 10, (32, 32, 3), [.7, .15, .15]),
    }

    if args.run_type == 'train':
        trainer = TrainExecutor(data_config=data_config, output_folder_name=args.result, epochs=args.epochs, lr=args.lr,
                                batch_size=args.batch)
        trainer.execute()
    elif args.run_type == 'eval':
        tester = TestExecutor(data_config=data_config, output_folder_name=args.result, lr=args.lr,
                              batch_size=args.batch)
        tester.execute()

    print('Done!')
