#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"

import argparse
import sys

import numpy as np
import pandas as pd

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--results', '-r', required=True, help='Results directory name')
    parser.add_argument('--output', '-o', required=True, help='Name of the output file where the results are saved')
    args = parser.parse_args()

    dataset_names = [
        'cats_vs_dogs_results.csv',
        'cifar10_results.csv',
        'fashion_mnist_results.csv',
        'mnist_results.csv',
        'cifar_haze_results.csv',
        'cad_haze_results.csv'
    ]

    sys.stdout = open(args.output, 'w')

    for dataset in dataset_names:
        try:
            df = pd.read_csv(
                '{}/{}'.format(args.results, dataset),
                usecols=['Model', 'Level', 'Test', 'Test_acc']
            )

            print('=' * 30)
            print(dataset)
            print('=' * 30)

            data = df.values
            models = np.unique(data[:, 0])
            levels = np.unique(data[:, 1])
            archs = np.unique(data[:, 2])

            arch_mean = dict()
            for arch in archs:
                curr_arch = str()
                contrast_lvl = 0
                for model in models:
                    accuracies = list()
                    for level in levels:
                        for row in data:
                            m, l, a, v = row
                            if model == m and level == l and arch == a:
                                accuracies.append(v)
                                curr_arch = arch
                    if accuracies:
                        group = int(curr_arch[1])
                        mean = np.mean(accuracies)
                        var = np.var(accuracies)
                        arch_mean[(curr_arch, contrast_lvl, group)] = (mean, var)
                        contrast_lvl += 1

            grouped_values = list()
            for key, value in arch_mean.items():
                arch, alpha, group = key
                mean, var = value
                grouped_values.append([arch, alpha, group, mean, var])

            means = pd.DataFrame(grouped_values, columns=['Architecture', 'CL', 'Group', 'Mean', 'Var'])
            means = means.sort_values(['Group', 'CL', 'Architecture'])

            print('{:^8} {:^8} {:^8} {:^8} {:^8} {:^8} {:^9} {:^8}'.format('C Arch', 'C Mean', 'C Var', 'M6 Arch',
                                                                           'M6 Mean', 'M6 Var', 'Level', 'Q'))
            even_count = 1
            prev_mean = 1.
            prev_var = 1.
            for row in means.iterrows():
                index, (arch, cl, grp, mean, var) = row
                if even_count % 2 == 0:
                    q = mean / prev_mean
                    even_count = 1
                    cl = 'C' + str(cl)
                    print('{:^8} {:^8.2f} {:8.2E} {:^8} {:^8.2f} {:8.2E} {:^9} {:^8.2f}'.format(prev_arch, prev_mean,
                                                                                                prev_var, arch, mean,
                                                                                                var, cl, q))
                    continue
                prev_mean = mean
                prev_var = var
                prev_arch = arch
                prev_bl = cl
                even_count += 1
        except FileNotFoundError:
            print('File {} not found.'.format(dataset))
