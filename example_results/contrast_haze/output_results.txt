File cats_vs_dogs_results.csv not found.
File cifar10_results.csv not found.
File fashion_mnist_results.csv not found.
File mnist_results.csv not found.
==============================
cifar_haze_results.csv
==============================
 C Arch   C Mean   C Var   M6 Arch  M6 Mean   M6 Var    Level      Q    
  A1C      0.38   3.35E-02   A1M6     0.49   2.96E-04    C0       1.29  
  A1C      0.41   3.03E-02   A1M6     0.50   9.82E-05    C1       1.24  
  A1C      0.37   2.11E-02   A1M6     0.50   7.63E-05    C2       1.35  
  A1C      0.28   6.02E-03   A1M6     0.49   1.64E-04    C3       1.76  
  A2C      0.36   3.31E-02   A2M6     0.53   2.97E-04    C0       1.47  
  A2C      0.41   3.13E-02   A2M6     0.53   1.18E-04    C1       1.27  
  A2C      0.38   2.01E-02   A2M6     0.49   9.63E-05    C2       1.27  
  A2C      0.28   7.89E-03   A2M6     0.51   2.26E-05    C3       1.82  
  A3C      0.39   3.80E-02   A3M6     0.56   2.78E-04    C0       1.43  
  A3C      0.41   3.79E-02   A3M6     0.61   2.06E-04    C1       1.50  
  A3C      0.35   2.90E-02   A3M6     0.60   1.12E-04    C2       1.70  
  A3C      0.26   4.17E-02   A3M6     0.59   7.85E-05    C3       2.30  
==============================
cad_haze_results.csv
==============================
 C Arch   C Mean   C Var   M6 Arch  M6 Mean   M6 Var    Level      Q    
  A1C      0.56   2.90E-03   A1M6     0.66   9.48E-05    C0       1.19  
  A1C      0.58   3.05E-03   A1M6     0.70   1.25E-04    C1       1.21  
  A1C      0.57   9.83E-05   A1M6     0.68   1.71E-04    C2       1.20  
  A1C      0.58   1.25E-05   A1M6     0.67   1.97E-04    C3       1.16  
  A2C      0.57   3.90E-03   A2M6     0.71   1.89E-04    C0       1.24  
  A2C      0.60   4.50E-03   A2M6     0.70   5.01E-05    C1       1.16  
  A2C      0.59   3.74E-03   A2M6     0.67   4.56E-05    C2       1.13  
  A2C      0.49   0.00E+00   A2M6     0.73   2.59E-05    C3       1.49  
  A3C      0.65   1.36E-02   A3M6     0.76   3.72E-04    C0       1.18  
  A3C      0.67   1.00E-02   A3M6     0.78   3.11E-05    C1       1.15  
  A3C      0.61   8.81E-03   A3M6     0.78   4.21E-05    C2       1.27  
  A3C      0.62   3.02E-03   A3M6     0.77   2.13E-05    C3       1.24  
