#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"


import models.convconvmodel
import models.convmodel
import models.m6model
import models.m6convmodel
import models.resnet20model


class ModelFactory:

    CONV_TEST = 'A1C'
    CONV_CONV_TEST = 'A2C'
    M6_TEST = 'A1M6'
    M6_CONV_TEST = 'A2M6'
    RESNET20_TEST = 'A3C'
    M6_RESNET20_TEST = 'A3M6'

    @staticmethod
    def instance(test_type, input_shape, num_classes):
        if test_type == ModelFactory.CONV_TEST:
            return models.convmodel.get_model(input_shape=input_shape, num_classes=num_classes)
        elif test_type == ModelFactory.M6_TEST:
            return models.m6model.get_model(input_shape=input_shape, num_classes=num_classes)
        elif test_type == ModelFactory.CONV_CONV_TEST:
            return models.convconvmodel.get_model(input_shape=input_shape, num_classes=num_classes)
        elif test_type == ModelFactory.M6_CONV_TEST:
            return models.m6convmodel.get_model(input_shape=input_shape, num_classes=num_classes)
        elif test_type == ModelFactory.RESNET20_TEST:
            return models.resnet20model.get_model(input_shape=input_shape, depth=20, num_classes=num_classes)
        elif test_type == ModelFactory.M6_RESNET20_TEST:
            return models.resnet20model.get_model(input_shape=input_shape, depth=20, num_classes=num_classes, m6_layer=True)
        return None

