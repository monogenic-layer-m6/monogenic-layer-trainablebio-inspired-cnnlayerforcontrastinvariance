#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sebastian Salazar Colores", "Sabastia Xambo", "Abraham Sanchez", "Ulises Cortes"]
__copyright__ = "Copyright 2020, Barcelona Supercomputing Center, Universidad Autonoma de Queretaro, Universitat Politecnica de Catalunya"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = ["dr.ulisesmoya@gmail.com", "ab.sanchezperez@gmail.com"]
__status__ = "Development"


import numpy as np

from tensorflow.keras.callbacks import Callback


class M6CallBack(Callback):
    
    def __init__(self, model):
        super(M6CallBack).__init__()
        self.weights = list()
        self.layer = None
        try:
            self.layer = model.get_layer('m6')
            s, mw, f, sigma = self.layer.get_weights()
            self.weights.append([0, float(s), float(mw), float(f), float(sigma)])
        except:
            print("[Callback] - M6 layer not found. Discarding callback's actions for this model.")

    def on_epoch_end(self, epoch, logs=None):
        if self.layer is not None:
            s, mw, f, sigma = self.layer.get_weights()
            self.weights.append([epoch+1, float(s), float(mw), float(f), float(sigma)])

    def get_weights(self):
        return np.array(self.weights)

